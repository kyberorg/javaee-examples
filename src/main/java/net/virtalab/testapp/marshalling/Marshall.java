package net.virtalab.testapp.marshalling;

import net.virtalab.testapp.types.PersonT;

import javax.xml.bind.*;
import java.io.File;

public class Marshall {
    public static void run(){
        JAXBContext ctx = null;
        try {
            ctx = JAXBContext.newInstance("net.virtalab.testapp.types");
            //System.out.println(ctx);
            Unmarshaller um = ctx.createUnmarshaller();
            Object o = um.unmarshal(new File("C:/code/testapp/src/main/resources/p2.xml"));
            System.out.println(o);
            //actions with object
            JAXBElement jaxbObject = (JAXBElement) o;
            PersonT person = (PersonT) jaxbObject.getValue();

            System.out.println("Old name: "+person.getFn());
            person.setFn("Raimo");
            System.out.println("New name: "+person.getFn());

            //marshaling
            Marshaller m = ctx.createMarshaller();
            //marshaller setup
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
            //save (marshall)
            m.marshal(jaxbObject, new File("C:/code/testapp/src/main/resources/result.xml"));

        } catch (JAXBException e) {
            System.err.println();
        }

    }


}
