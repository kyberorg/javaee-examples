package net.virtalab.testapp;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class SAXParser {
    static final String JAXP_SCHEMA_LANGUAGE="http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    static final String W3C_XML_SCHEMA="http://www.w3.org/2001/XMLSchema";
    static final String JAXP_SCHEMA_SOURCE="http://java.sun.com/xml/jaxp/properties/schemaSource";
    public static void parse(){
        File f = new File("C:\\code\\testapp\\src\\main\\resources\\p2.xml");
        SAXParserFactory factory = SAXParserFactory.newInstance();
        javax.xml.parsers.SAXParser parser;

        try {
            factory.setNamespaceAware(true);
            factory.setValidating(true);

            parser = factory.newSAXParser();
            parser.setProperty(JAXP_SCHEMA_LANGUAGE,W3C_XML_SCHEMA);
            parser.setProperty(JAXP_SCHEMA_SOURCE,"C:\\code\\testapp\\src\\main\\resources\\Person.xsd");

            parser.parse(f, new MyHandler());
            System.out.println("Doc is valid...");
        } catch (Exception e) {
            System.err.println(e);
        }





    }
}
