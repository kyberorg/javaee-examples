package net.virtalab.testapp;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class MyHandler extends DefaultHandler {
    @Override
    public void warning(SAXParseException e) throws SAXException {
        System.err.println("FATAL ERROR: "+e);
        System.exit(-1);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        System.err.println("ERROR: "+e);
        System.exit(-1);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        System.err.println("WARN: "+e);
        System.exit(-1);
    }
}
