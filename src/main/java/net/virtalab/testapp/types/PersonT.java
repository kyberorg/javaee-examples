
package net.virtalab.testapp.types;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PersonT complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonT">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sn" type="{http://virtalab.net/xml/Person}MyString"/>
 *         &lt;element name="fn" type="{http://virtalab.net/xml/Person}MyString"/>
 *         &lt;element name="address" type="{http://virtalab.net/xml/Person}AddressT"/>
 *         &lt;element name="sex" type="{http://virtalab.net/xml/Person}SexT"/>
 *         &lt;element name="children" type="{http://virtalab.net/xml/Person}ChildlenT" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonT", propOrder = {
    "sn",
    "fn",
    "address",
    "sex",
    "children"
})
public class PersonT {

    @XmlElement(required = true)
    protected String sn;
    @XmlElement(required = true)
    protected String fn;
    @XmlElement(required = true)
    protected AddressT address;
    @XmlElement(required = true)
    protected SexT sex;
    @XmlElement(required = true)
    protected List<ChildlenT> children;

    /**
     * Gets the value of the sn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSn() {
        return sn;
    }

    /**
     * Sets the value of the sn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSn(String value) {
        this.sn = value;
    }

    /**
     * Gets the value of the fn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFn() {
        return fn;
    }

    /**
     * Sets the value of the fn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFn(String value) {
        this.fn = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link AddressT }
     *     
     */
    public AddressT getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressT }
     *     
     */
    public void setAddress(AddressT value) {
        this.address = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link SexT }
     *     
     */
    public SexT getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link SexT }
     *     
     */
    public void setSex(SexT value) {
        this.sex = value;
    }

    /**
     * Gets the value of the children property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the children property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChildren().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChildlenT }
     * 
     * 
     */
    public List<ChildlenT> getChildren() {
        if (children == null) {
            children = new ArrayList<ChildlenT>();
        }
        return this.children;
    }

}
