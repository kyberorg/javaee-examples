
package net.virtalab.testapp.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.virtalab.testapp.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Person_QNAME = new QName("http://virtalab.net/xml/Person", "person");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.virtalab.testapp.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PersonT }
     * 
     */
    public PersonT createPersonT() {
        return new PersonT();
    }

    /**
     * Create an instance of {@link AddressT }
     * 
     */
    public AddressT createAddressT() {
        return new AddressT();
    }

    /**
     * Create an instance of {@link ChildlenT }
     * 
     */
    public ChildlenT createChildlenT() {
        return new ChildlenT();
    }

    /**
     * Create an instance of {@link SexT }
     * 
     */
    public SexT createSexT() {
        return new SexT();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonT }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://virtalab.net/xml/Person", name = "person")
    public JAXBElement<PersonT> createPerson(PersonT value) {
        return new JAXBElement<PersonT>(_Person_QNAME, PersonT.class, null, value);
    }

}
