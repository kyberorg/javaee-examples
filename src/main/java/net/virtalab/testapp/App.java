package net.virtalab.testapp;

import net.virtalab.testapp.marshalling.Marshall;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	SAXParser.parse();
        Marshall.run();
    }
}
